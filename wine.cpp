/* Wesley Fisher - hw3 - 11/15/15*/

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <math.h>
#include <vector>
#include <algorithm>
using namespace std;

struct Node{
	double key[11];
	double label;
	Node *left;
	Node *right;
	Node(double *val, double l){
		for(int i = 0; i < 11; i++) key[i] = val[i];
		label = l;
		left = NULL;
		right = NULL;
	}
	Node(){
		for(int i = 0; i < 11; i++) key[i] = 0;
		label = 0.0;
		left = NULL;
		right = NULL;
	}
};

typedef pair<double,Node*> Pair;
vector<Pair> V;
int N, D;

Node *insert(Node *T, double *val, double l, int d){

	if(T == NULL) return new Node(val, l);
	if(val[d] < T->key[d]) T->left = insert(T->left, val, l, (d+1)%D);
	else T->right = insert(T->right, val, l, (d+1)%D);
	return T;

}

Node *find(Node *T, double *val, int d){

	if (T == NULL) return NULL;
	if (val[d] == T->key[d]) return T;
	if (val[d] < T->key[d]) return find(T->left, val, (d+1)%D);
	else return find(T->right, val, (d+1)%D);

}

double node_distance(Node *A, Node *B){

	double dist = 0.0;
	for(int i = 0; i < D; i++) dist += pow(A->key[i] - B->key[i],2);	

	return sqrt(dist);

}

void distance_inorder(Node *T, Node *A){

	if(T == NULL) return;
	distance_inorder(T->left, A);
	if(node_distance(T, A) != 0.0){
		Pair pair;
		pair.first = node_distance(T, A);
		pair.second = T;
		V.push_back(pair);
	}
	distance_inorder(T->right, A);

}

int main(int argc, char *argv[]){

	if(argc != 3){
		cout << "File name and/or value of k missing.\n";
		return 0;
	}

	int k = atoi(argv[2]);

	if(k < 1 || k > 10){
		cout << "Your k value is invalid.\n";
		return 0;
	}

	//STEP 1: Read Input
	ifstream fin (argv[1]);

	//Read in number of wines and number of "dimensions"
	fin >> N;
	fin >> D;

	double *reallabels = new double[N];

	double **dimensions = new double*[N];
	for(int i = 0; i < N; i++) dimensions[i] = new double[D];
	
	//Read in information for all of the wines
	for(int i = 0; i < N; i++){
		for(int j = 0; j <= D; j++){
			if(j == 0) fin >> reallabels[i];
			else fin >> dimensions[i][j-1];
		}
	}

	fin.close();



	//STEP 2: Convert Input
	//Convert the values in dimensions to the Z domain
	for(int j = 0; j < D; j++){
		double mean;
		double variance;
		double standarddev;

		for(int i = 0; i < N; i++)
			mean += dimensions[i][j];
		mean /= N;

		for(int i = 0; i < N; i++)
			variance += pow((dimensions[i][j] - mean), 2);
		variance /= N-1;
		standarddev = sqrt(variance);

		for(int i = 0; i < N; i++)
			dimensions[i][j] = (dimensions[i][j] - mean) / standarddev;

	}

	//Build the rescaled data into a kd tree
	Node *T = new Node();

	for(int i = 0; i < N; i++) T = insert(T, dimensions[i], reallabels[i], 0);



	//STEP 3: Classification
	double *newlabels = new double[N];

	/*double average = 0.0;

	for(int i = 0; i < N; i++) average += reallabels[i];
	average /= N;

	for(int i = 0; i < N; i++) newlabels[i] = average;*/

	for(int i = 0; i < N; i++){

		//Find k nearest neighbors
		V.clear();
		Node *neighbors[k];
		Node *A = find(T, dimensions[i], 0);

		distance_inorder(T, A);

		sort(V.begin(), V.end());
		for(int z = 0; z < k; z++){
			neighbors[z] = V[z].second;
		}

		//Estimate label based on weighted average of neighbors
		double numerator = 0.0;
		double denomenator = 0.0;

		for(int j = 0; j < k; j++){
			double weight = exp((-1)*node_distance(A, neighbors[j]));
			numerator += (weight * neighbors[j]->label);
			denomenator += weight;
		}

		newlabels[i] = numerator / denomenator;

	}


	//STEP 4: Print out error of label estimates
	double error = 0.0;
	for(int i = 0; i < N; i++) error += pow(reallabels[i] - newlabels[i], 2);
	error /= N;

	cout << error << endl;

	return 0;
}
